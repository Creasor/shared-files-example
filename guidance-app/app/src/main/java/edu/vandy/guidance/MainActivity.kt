package edu.vandy.guidance

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.FileProvider.getUriForFile
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import edu.vandy.guidance.ui.theme.AppTheme
import java.io.File
import kotlin.math.min

class MainActivity : ComponentActivity() {
    companion object {
        const val GUIDANCE_FILE = "guidance.txt"
        const val SQI_FILE = "sqi.txt"
    }

    private val sharedViewModel: SharedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedViewModel.setIntent(intent)

        setContent {
            AppTheme {
                Surface {
                    val uiState by sharedViewModel.uiState.collectAsStateWithLifecycle()

                    // Create some example sample log files to share
                    // with calling apps.
                    createSampleFiles(applicationContext)

                    MainScreen(
                        uiState = uiState,
                        onButtonClicked = {
                            setResult(
                                RESULT_OK,
                                createSharedFilesIntent(
                                    this,
                                    listOf(GUIDANCE_FILE, SQI_FILE),
                                    applicationContext.packageName
                                )
                            )
                            finish()
                        }
                    )
                }
            }
        }
    }

    override fun setIntent(newIntent: Intent?) {
        super.setIntent(newIntent)
        if (newIntent != null) {
            sharedViewModel.setIntent(newIntent)
        }
    }

    private fun getAppExternalFile(fileName: String): File =
        File(getExternalFilesDir(null), fileName)

    private fun createSharedFilesIntent(
        context: Context,
        fileNames: List<String>,
        authority: String
    ): Intent =
        Intent().apply {
            action = Intent.ACTION_SEND_MULTIPLE
            type = "text/plain"
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            clipData = fileNames.map { fileName ->
                ClipData.Item(getUriForFile(context, authority, getAppExternalFile(fileName)))
            }.fold(null as ClipData?) { clipData, item ->
                clipData?.apply { addItem(item) } ?: ClipData(null, arrayOf("text/plain"), item)
            }
        }
}

@Composable
fun MainScreen(
    uiState: UiState,
    onButtonClicked: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Card(
                modifier = Modifier.fillMaxWidth()
            ) {
                InputArea(
                    uiState = uiState,
                )
            }

            Card(
                modifier = Modifier.fillMaxWidth()
            ) {
                OutputArea(
                    files = listOf(MainActivity.GUIDANCE_FILE, MainActivity.SQI_FILE),
                )
            }
        }

        Button(
            onClick = onButtonClicked,
            modifier = Modifier.align(Alignment.BottomCenter)
        ) {
            if (uiState.invokedBy == InvokedBy.Launcher) {
                Text("Exit")
            } else {
                Text("Share and Return")
            }
        }
    }
}

@Composable
private fun InputArea(
    uiState: UiState,
) {
    val startedBy = when (uiState.invokedBy) {
        InvokedBy.BrainGamesApp -> "Started by Brain Games"
        InvokedBy.MultiStreamApp -> "Started by MultiStream"
        InvokedBy.Launcher -> "Started by Launcher"
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = startedBy,
                modifier = Modifier
                    .padding(bottom = 16.dp)
                    .align(Alignment.CenterHorizontally),
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                color = MaterialTheme.colorScheme.primary
            )

            uiState.intent?.extras?.let {
                Text(
                    text = "Input",
                    modifier = Modifier.padding(16.dp)
                )

                IntentExtras(it)
            } ?: Text(
                text = "No input",
                modifier = Modifier.padding(16.dp)
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun IntentExtras(extras: Bundle) {

    for (key in extras.keySet()) {
        when (key) {
            BrainGamesKeys.DeviceMac.key -> {
                extras.getString(key)
            }

            MultiStreamKeys.SelectedSNRChannels.key -> {
                val value = extras.getBooleanArray(key) ?: BooleanArray(0)
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "$key:")
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        text = value.contentToString(),
                        modifier = Modifier.basicMarquee(),
                        maxLines = 1
                    )
                }
            }

            MultiStreamKeys.SqiThrSumHbRatio.key,
            MultiStreamKeys.SqiThrUpIntensity.key,
            MultiStreamKeys.SqiThrLowIntensity.key -> {
                val value = extras.getDouble(key, 0.0)
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "$key:")
                    Text(text = "%.1f".format(value))
                }
            }

            else -> {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "$key:")
                    Text(text = "Unknown value")
                }
            }
        }
    }
}

@Composable
fun OutputArea(
    files: List<String>
) {
    Column(
        modifier = Modifier.padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        files.forEach { fileName ->
            FileArea(
                fileName = fileName,
            )
        }
    }
}

@Composable
fun FileArea(
    fileName: String
) {
    val context = LocalContext.current
    var content by remember { mutableStateOf("") }
    var showFileContentsScreen by remember { mutableStateOf(false) }
    val horizontalScrollState = rememberScrollState()
    val verticalScrollState = rememberScrollState()

    LaunchedEffect(Unit) {
        val file = File(context.getExternalFilesDir(null), fileName)
        if (file.exists()) {
            content = file.readText()
        }
    }

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(text = fileName)

        val lineCount = content.lines().count()

        Text(
            text = content,
            modifier = Modifier
                .height(24.dp * min(8, lineCount))
                .fillMaxWidth()
                .horizontalScroll(horizontalScrollState)
                .verticalScroll(verticalScrollState)
        )

        Button(
            onClick = {
                showFileContentsScreen = true
            },
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
        ) {
            Text(
                text = "View",
                maxLines = 1
            )
        }
    }

    if (showFileContentsScreen) {
        FileContentsDialog(
            title = fileName,
            content = content,
            onClosed = {
                showFileContentsScreen = false
            }
        )
    }
}

@Composable
fun FileContentsDialog(
    title: String,
    content: String,
    onClosed: () -> Unit
) {
    val horizontalScrollState = rememberScrollState()
    val verticalScrollState = rememberScrollState()

    AlertDialog(
        onDismissRequest = onClosed,
        title = { Text(text = title) },
        text = {
            Text(
                text = content,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .horizontalScroll(horizontalScrollState)
                    .verticalScroll(verticalScrollState)
            )
        },

        confirmButton = {
            Button(onClick = onClosed) {
                Text("Close")
            }
        }
    )
}

private fun createSampleFiles(context: Context) {
    val dir = context.getExternalFilesDir(null)
    val guidanceFile = File(dir, MainActivity.GUIDANCE_FILE)
    val sqiFile = File(dir, MainActivity.SQI_FILE)

    var guidanceLogData = AlignmentData.fileHeaders
    repeat(10) {
        guidanceLogData += AlignmentData.generateRandomData().toCsvString()
    }

    guidanceFile.writeText(guidanceLogData)

    val numChannels = 33
    val content = StringBuilder()
    for (i in 1..numChannels) {
        content.append("ch").append(i).append("R,")
            .append("ch").append(i).append("IR,")
            .append("ch").append(i).append("DC,")
            .append("ch").append(i).append("R_SNR,")
            .append("ch").append(i).append("IR_SNR,")
            .append("ch").append(i).append("_Status,")
    }

    content.append(System.lineSeparator())
    content.append("SQI does not currently log any data.")
    sqiFile.writeText(content.toString())
}


@Preview
@Composable
fun PreviewMainScreen() {
    AppTheme {
        Surface {
            createSampleFiles(LocalContext.current)
            MainScreen(
                uiState = UiState(),
                onButtonClicked = {}
            )
        }
    }
}