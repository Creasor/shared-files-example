package edu.vandy.guidance

import androidx.compose.runtime.Stable
import androidx.compose.ui.geometry.Offset
import java.util.Calendar
import java.util.TimeZone
import kotlin.random.Random

@Stable
data class AlignmentData(
    val nasionInionDistanceMm: Float = 0f,
    val pupillaryDistanceMm: Float = 0f,
    val nasionEyebrowDistanceMm: Float = 0f,
    val markerTopLeft: Offset = Offset.Zero,
    val markerTopRight: Offset = Offset.Zero,
    val markerBottomLeft: Offset = Offset.Zero,
    val markerBottomRight: Offset = Offset.Zero,
    val nasion: Offset = Offset.Zero,
    val leftEyeCenter: Offset = Offset.Zero,
    val rightEyeCenter: Offset = Offset.Zero,
    val leftEyebrowApex: Offset = Offset.Zero,
    val rightEyebrowApex: Offset = Offset.Zero,
    val rotationError: Float = 0f,
    val translationError: Offset = Offset.Zero
) {
    fun toCsvString(): String =
        StringBuilder().apply {
            val cal = Calendar.getInstance(TimeZone.getDefault())
            append("${cal[Calendar.YEAR]},")
            append("${cal[Calendar.MONTH]},")
            append("${cal[Calendar.DAY_OF_MONTH]},")
            append("${cal[Calendar.HOUR_OF_DAY]},")
            append("${cal[Calendar.MINUTE]},")
            append("${cal[Calendar.SECOND]},")
            append("${nasionInionDistanceMm.toInt()},")
            append("${pupillaryDistanceMm.toInt()},")
            append("${nasionEyebrowDistanceMm.toInt()},")
            append("${markerTopLeft.x.toInt()},${markerTopLeft.y.toInt()},")
            append("${markerTopRight.x.toInt()},${markerTopRight.y.toInt()},")
            append("${markerBottomLeft.x.toInt()},${markerBottomLeft.y.toInt()},")
            append("${markerBottomRight.x.toInt()},${markerBottomRight.y.toInt()},")
            append("${nasion.x.toInt()},${nasion.y.toInt()},")
            append("${leftEyeCenter.x.toInt()},${leftEyeCenter.y.toInt()},")
            append("${rightEyeCenter.x.toInt()},${rightEyeCenter.y.toInt()},")
            append("${leftEyebrowApex.x.toInt()},${leftEyebrowApex.y.toInt()},")
            append("${rightEyebrowApex.x.toInt()},${rightEyebrowApex.y.toInt()},")
            append("${rotationError.toInt()},")
            append("${translationError.x.toInt()}, ${translationError.y.toInt()}")
            append(System.lineSeparator())
        }.toString()

    companion object {
        val fileHeaders: String = StringBuilder().apply {
            append("date_YYYY,")
            append("dateMM,")
            append("dateDD,")
            append("dataHH,")
            append("dateMin,")
            append("dateSS,")
            append("nasionInionDistanceMm,")
            append("nasionEyebrowDistanceMm,")
            append("pupillaryDistanceMm,")
            append("markerTopLeftX, markerTopLeftY,")
            append("markerTopRightX, markerTopRightY,")
            append("markerBottomLeftX, markerBottomLeftY,")
            append("markerBottomRightX, markerBottomRightY,")
            append("nasionX, nasionY,")
            append("leftEyeCenterX, leftEyeCenterY,")
            append("rightEyeCenterX, rightEyeCenterY,")
            append("leftEyebrowApexX, leftEyebrowApexY,")
            append("rightEyebrowApexX, rightEyebrowApexY,")
            append("rotationError, translationError")
            append(System.lineSeparator())
        }.toString()

        fun generateRandomData(): AlignmentData {
            return AlignmentData(
                nasionInionDistanceMm = Random.nextInt(300, 380).toFloat(),
                pupillaryDistanceMm = Random.nextInt(52, 68).toFloat(),
                nasionEyebrowDistanceMm = Random.nextInt(10, 20).toFloat(),
                markerTopLeft = randomOffset(300, 350),
                markerTopRight = randomOffset(300, 350),
                markerBottomLeft = randomOffset(300, 350),
                markerBottomRight = randomOffset(300, 350),
                nasion = randomOffset(400, 700),
                leftEyeCenter = randomOffset(100, 200),
                rightEyeCenter = randomOffset(300, 400),
                leftEyebrowApex = randomOffset(100, 200),
                rightEyebrowApex = randomOffset(300, 400),
                rotationError = Random.nextInt(0, 7).toFloat(),
                translationError = randomOffset(2, 10)
            )
        }

        private fun randomOffset(min: Int = 100, max: Int = 500): Offset =
            Offset(
                Random.nextInt(min, max).toFloat(),
                Random.nextInt(min, max).toFloat()
            )
    }
}