package edu.vandy.guidance

import android.app.Application
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.FileProvider.getUriForFile
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.io.File

/**
 * Enum class that defines the [Intent] extra keys
 * used by the Multi-stream app to pass data to the
 * guidance app. To avoid errors, be sure to use the
 * enumerated type key values (not strings) when
 * creating the [Intent] extras. This also allows the
 * 3 apps to be consistent in the data they pass by
 * all apps using the exact same enum class.
 */
enum class MultiStreamKeys(val key: String) {
    SelectedSNRChannels("selected-snr-channels"),
    SqiThrSumHbRatio("sqi-sum-hb-ratio"),
    SqiThrUpIntensity("sqi-od-max"),
    SqiThrLowIntensity("sqi-od-min"),
}

/**
 * Enum class that defines the [Intent] extra keys used
 * by the BrainGames app to pass data to the guidance app.
 * To avoid errors, be sure to use the enumerated type key
 * values (not strings) when creating the [Intent] extras.
 */
enum class BrainGamesKeys(val key: String) {
    DeviceMac("deviceMac")
}

enum class InvokedBy {
    BrainGamesApp,
    MultiStreamApp,
    Launcher,
}

data class InputData(
    @Suppress("ArrayInDataClass")
    val channelsToIgnore: BooleanArray? = null,
    val sqiThrSumHbRatio: Double? = null,
    val sqiThrUpIntensity: Double? = null,
    val sqiThrLowIntensity: Double? = null,
    val deviceMac: String? = null,
)

data class UiState(
    val invokedBy: InvokedBy = InvokedBy.Launcher,
    val inputData: InputData? = null,
    val intent: Intent? = null,
)

class SharedViewModel(application: Application) : AndroidViewModel(application) {
    private var intent: Intent? = null

    private val _uiState = MutableStateFlow(UiState())
    val uiState = _uiState.asStateFlow()

    private val invokedBy: InvokedBy
        get() = when {
            startedByLauncher -> InvokedBy.Launcher
            startedByBrainGamesApp -> InvokedBy.BrainGamesApp
            startedByMultiStreamApp -> InvokedBy.MultiStreamApp
            else -> InvokedBy.Launcher
        }

    private val startedByBrainGamesApp: Boolean
        get() = intent?.extras?.containsKey(BrainGamesKeys.DeviceMac.key) == true

    private val startedByMultiStreamApp: Boolean
        get() =
            intent?.extras?.run {
                containsKey(MultiStreamKeys.SelectedSNRChannels.key) &&
                containsKey(MultiStreamKeys.SqiThrSumHbRatio.key) &&
                containsKey(MultiStreamKeys.SqiThrUpIntensity.key) &&
                containsKey(MultiStreamKeys.SqiThrLowIntensity.key)
            } ?: false

    private val startedByLauncher: Boolean
        get() = intent?.let {
            it.action == Intent.ACTION_MAIN && it.hasCategory(Intent.CATEGORY_LAUNCHER)
        } == true

    fun setIntent(intent: Intent) {
        this.intent = intent
        extractIntentExtras(intent.extras)
        _uiState.update { state ->
            state.copy(
                intent = intent,
                invokedBy = invokedBy,
            )
        }
    }

    private fun extractIntentExtras(extras: Bundle?) {
        viewModelScope.launch {
            extras?.run {
                when (invokedBy) {
                    InvokedBy.BrainGamesApp -> {
                        _uiState.update { state ->
                            val deviceMac = getString("deviceMac")
                            if (isValidMacAddress(deviceMac)) {
                                state.copy(inputData = InputData(deviceMac = deviceMac))
                            } else {
                                state.copy(inputData = InputData(deviceMac = "Invalid MAC address"))
                            }
                        }
                    }

                    InvokedBy.MultiStreamApp -> {
                        val channelsToIgnore =
                            getBooleanArray(MultiStreamKeys.SelectedSNRChannels.key)
                        val sqiThrSumHbRatio =
                            getDouble(MultiStreamKeys.SqiThrSumHbRatio.key, Double.MAX_VALUE)
                        val sqiThrUpIntensity =
                            getDouble(MultiStreamKeys.SqiThrUpIntensity.key, Double.MAX_VALUE)
                        val sqiThrLowIntensity =
                            getDouble(MultiStreamKeys.SqiThrLowIntensity.key, Double.MAX_VALUE)

                        _uiState.update { state ->
                            state.copy(
                                inputData = InputData(
                                    channelsToIgnore = channelsToIgnore,
                                    sqiThrSumHbRatio = sqiThrSumHbRatio,
                                    sqiThrUpIntensity = sqiThrUpIntensity,
                                    sqiThrLowIntensity = sqiThrLowIntensity,
                                )
                            )
                        }
                    }

                    InvokedBy.Launcher -> {
                        Unit
                    }
                }
            }
        }
    }

    private fun isValidMacAddress(deviceMac: String?): Boolean =
        deviceMac?.matches(
            Regex("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
        ) == true

    /**
     * Returns an intent that shares the guidance and sqi data files
     * when returning from an invoking app. The files are stored in
     * this app's private external storage but are made available to
     * the calling apps via a content provider.
     */
    fun getSharedFilesIntent(): Intent {
        val fileNames = listOf("guidance.csv", "sqi.csv")
        val authority = getApplication<Application>().packageName

        return createSharedFilesIntent(getApplication(), fileNames, authority).also {
            getApplication<Application>().startActivity(it)
        }
    }

    /**
     * Creates an intent that shares the files with the
     * given [fileNames] using the given [authority].
     */
    private fun createSharedFilesIntent(
        context: Context,
        fileNames: List<String>,
        authority: String
    ): Intent =
        Intent().apply {
            action = Intent.ACTION_SEND_MULTIPLE
            type = "text/plain"

            // Allow the calling app to read the files.
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

            // Add each file Uri to the Intent's clip data.
            clipData = fileNames.map { fileName ->
                ClipData.Item(getUriForFile(context, authority, getAppExternalFile(fileName)))
            }.fold(null as ClipData?) { clipData, item ->
                clipData?.apply { addItem(item) } ?: ClipData(null, arrayOf("text/plain"), item)
            }
        }

    /**
     * Returns [File] object for the specified [fileName]
     * located in this app's private external storage.
     */
    private fun getAppExternalFile(fileName: String): File =
        File(getApplication<Application>().getExternalFilesDir(null), fileName)
}