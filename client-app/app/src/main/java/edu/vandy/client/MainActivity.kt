package edu.vandy.client

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.vandy.client.ui.theme.AppTheme
import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.min
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    private var sharedFiles by mutableStateOf(emptyList<Pair<String, String>>())

    /**
     * Activity result launcher used to start the
     * guidance test app and receive its shared files.
     */
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                // A list of shared files paths are passed in Intent clipData
                result.data?.clipData?.let { clipData ->
                    val files = mutableListOf<Pair<String, String>>()

                    // Process each shared file extracting its name and
                    // content and add them to the list of files to be
                    // displayed in the UI.
                    for (i in 0 until clipData.itemCount) {
                        val uri = clipData.getItemAt(i).uri
                        val content = try {
                            contentResolver.openInputStream(uri)?.use { inputStream ->
                                val reader = BufferedReader(InputStreamReader(inputStream))
                                reader.readText()
                            } ?: throw Exception("Provider not responding")
                        } catch (e: Exception) {
                            "Failed to read file: ${e.message}"
                        }
                        files.add(
                            Pair(
                                uri.lastPathSegment ?: "Unknown",
                                content
                            )
                        )
                    }
                    sharedFiles = files
                } ?: run {
                    sharedFiles = listOf("No files found" to "")
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            AppTheme {
                Surface {
                    MainScreen(
                        brainGames = AppData.BrainGames(),
                        multiStream = AppData.MultiStream(),
                        sharedContent = sharedFiles,
                        onStartClicked = { data -> startGuidanceApp(data.intent) }
                    )
                }
            }
        }
    }

    private fun startGuidanceApp(intent: Intent) {
        startForResult.launch(intent)
    }

}

/**
 * The package name and main activity name that must be
 * used when creating any intent to start the Guidance app.
 */
private const val guidanceAppPackageName = "edu.vandy.guidance"
private const val guidanceAppMainActivity = "edu.vandy.guidance.MainActivity"

/**
 * Enum class that defines the [Intent] extra keys
 * used by the Multi-stream app to pass data to the
 * guidance app. To avoid errors, be sure to use the
 * enumerated type key values (not strings) when
 * creating the [Intent] extras. This also allows the
 * 3 apps to be consistent in the data they pass by
 * all apps using the exact same enum class.
 */
private enum class MultiStreamKeys(val key: String) {
    SelectedSNRChannels("selected-snr-channels"),
    SqiThrSumHbRatio("sqi-sum-hb-ratio"),
    SqiThrUpIntensity("sqi-od-max"),
    SqiThrLowIntensity("sqi-od-min"),
}

/**
 * Enum class that defines the [Intent] extra keys used
 * by the BrainGames app to pass data to the guidance app.
 * To avoid errors, be sure to use the enumerated type key
 * values (not strings) when creating the [Intent] extras.
 */
private enum class BrainGamesKeys(val key: String) {
    DeviceMac("deviceMac"),
}

/**
 * Sealed class that wraps the [Intent] that can be sent
 * to the Guidance app from the BrainGames and MultiStream
 * apps. This class is only used to generate random data
 * that simulates the data that would be passed by the
 * BrainGames and MultiStream apps to the Guidance app.
 */
private sealed class AppData(open val intent: Intent) {
    data class BrainGames(
        override val intent: Intent = generateRandomBrainGamesIntent()
    ) : AppData(intent)

    data class MultiStream(
        override val intent: Intent = generateRandomMultiStreamIntent()
    ) : AppData(intent)

    data class Launcher(
        override val intent: Intent = generateGuidanceAppIntent()
    ) : AppData(intent)
}

/**
 * Generates a MultiStream app Intent that that can be used
 * to start the Guidance app. The intent will contain a set
 * of random data that simulates the data that the MultiStream
 * app would pass to the Guidance app. Note that the randomly
 * generated data is within the ranges expected by the Guidance
 * app.
 */
private fun generateRandomMultiStreamIntent(): Intent =
    generateGuidanceAppIntent().apply {
        MultiStreamKeys.entries.forEach { key ->
            when (key) {
                MultiStreamKeys.SelectedSNRChannels -> putExtra(
                    key.key,
                    BooleanArray(33) { Random.nextBoolean() }
                )

                MultiStreamKeys.SqiThrSumHbRatio -> putExtra(
                    key.key,
                    Random.nextDouble(0.0, 5.0)
                )

                MultiStreamKeys.SqiThrUpIntensity -> putExtra(
                    key.key,
                    Random.nextDouble(0.0, 5.0)
                )

                MultiStreamKeys.SqiThrLowIntensity -> putExtra(
                    key.key,
                    Random.nextDouble(-5.0, 0.0)
                )
            }
        }
    }

/**
 * Generates a BrainGames app [Intent] that that can be used
 * to start the Guidance app. The intent will contain a set
 * of random data that simulates the data that the BrainGames
 * app would pass to the Guidance app. Note that the randomly
 * generated data is reasonable and conforms to the format
 * expected by the Guidance app.
 */
private fun generateRandomBrainGamesIntent(): Intent =
    generateGuidanceAppIntent().apply {
        putExtra(
            BrainGamesKeys.DeviceMac.key,
            List(6) {
                Random.nextInt(0, 256).toString(16)
            }.joinToString(":") {
                it.padStart(2, '0')
            }
        )
    }

/**
 * Generates a basic [Intent] that can be used to start the
 * Guidance app.
 */
private fun generateGuidanceAppIntent(): Intent =
    Intent().setClassName(
        guidanceAppPackageName,
        guidanceAppMainActivity
    )


@Composable
private fun MainScreen(
    brainGames: AppData.BrainGames,
    multiStream: AppData.MultiStream,
    sharedContent: List<Pair<String, String>>,
    onStartClicked: (AppData) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        item {
            Card(
                modifier = Modifier.fillMaxWidth(),
            ) {
                BrainGamesApp(
                    data = AppData.BrainGames(),
                    onStartClicked = {
                        onStartClicked(brainGames)
                    }
                )
            }
        }

        item {
            Card(
                modifier = Modifier.fillMaxWidth(),
            ) {
                MultiStreamApp(
                    data = AppData.MultiStream(),
                    onStartClicked = {
                        onStartClicked(multiStream)
                    }
                )
            }
        }

        if (sharedContent.isNotEmpty()) {
            item {
                Card(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    SharedFileResults(sharedContent)
                }
            }
        }
    }
}

@Composable
private fun BrainGamesApp(
    data: AppData.BrainGames,
    onStartClicked: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "BrainGames App",
                modifier = Modifier
                    .padding(bottom = 16.dp)
                    .align(Alignment.CenterHorizontally),
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                color = MaterialTheme.colorScheme.primary
            )

            Text(text = "Intent Data")

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                val key = BrainGamesKeys.DeviceMac.key
                val value = data.intent.getStringExtra(key) ?: ""
                Text(text = "$key:")
                Text(text = value)
            }

            Button(
                onClick = onStartClicked,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            ) {
                Text("Start Guidance App")
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun MultiStreamApp(
    data: AppData.MultiStream,
    onStartClicked: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "MultiStream App",
                modifier = Modifier
                    .padding(bottom = 16.dp)
                    .align(Alignment.CenterHorizontally),
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                color = MaterialTheme.colorScheme.primary
            )

            Text(text = "Intent Data")

            MultiStreamKeys.entries.forEach { key ->
                when (key) {
                    MultiStreamKeys.SelectedSNRChannels -> {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Text(text = "${key.key}:")
                            Spacer(modifier = Modifier.width(8.dp))
                            Text(
                                text = data.intent.getBooleanArrayExtra(key.key).contentToString(),
                                modifier = Modifier.basicMarquee(),
                                maxLines = 1
                            )
                        }
                    }

                    MultiStreamKeys.SqiThrSumHbRatio,
                    MultiStreamKeys.SqiThrUpIntensity,
                    MultiStreamKeys.SqiThrLowIntensity -> {
                        val value = data.intent.getDoubleExtra(key.key, 0.0)
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Text(text = "${key.key}:")
                            Text(text = "%.1f".format(value))
                        }
                    }
                }
            }

            Button(
                onClick = onStartClicked,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            ) {
                Text("Start Guidance App")
            }
        }
    }
}

@Composable
private fun SharedFileResults(
    sharedContent: List<Pair<String, String>>
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Shared Files",
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.primary
        )

        sharedContent.forEach { (name, content) ->
            FileArea(
                fileName = name,
                content = content
            )
        }
    }
}

@Composable
fun FileArea(
    fileName: String,
    content: String,
) {
    var showFileContentsScreen by remember { mutableStateOf(false) }
    val horizontalScrollState = rememberScrollState()
    val verticalScrollState = rememberScrollState()

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(text = fileName)

        val lineCount = content.lines().count()

        Text(
            text = content,
            modifier = Modifier
                .height(24.dp * min(8, lineCount))
                .fillMaxWidth()
                .horizontalScroll(horizontalScrollState)
                .verticalScroll(verticalScrollState)
        )

        Button(
            onClick = {
                showFileContentsScreen = true
            },
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
        ) {
            Text(
                text = "View",
                maxLines = 1
            )
        }
    }

    if (showFileContentsScreen) {
        FileContentsDialog(
            title = fileName,
            content = content,
            onClosed = {
                showFileContentsScreen = false
            }
        )
    }
}

@Composable
fun FileContentsDialog(
    title: String,
    content: String,
    onClosed: () -> Unit
) {
    val horizontalScrollState = rememberScrollState()
    val verticalScrollState = rememberScrollState()

    AlertDialog(
        onDismissRequest = onClosed,
        title = { Text(text = title) },
        text = {
            Text(
                text = content,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .horizontalScroll(horizontalScrollState)
                    .verticalScroll(verticalScrollState)
            )
        },

        confirmButton = {
            Button(onClick = onClosed) {
                Text("Close")
            }
        }
    )
}

@Preview
@Composable
private fun PreviewAppScreen() {
    AppTheme {
        Surface {
            MainScreen(
                brainGames = AppData.BrainGames(),
                multiStream = AppData.MultiStream(),
                sharedContent = emptyList(),
                onStartClicked = {}
            )
        }
    }
}